import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import ProfileScreen from './screens/ProfileScreen/ProfileScreen';
import Home from './screens/Home/Home'
import FormScreen from './screens/FormScreen/FormScreen';
import TableScreen from './screens/TableScreen/TableScreen';
import TableScreenTwo from './screens/TableScreenTwo/TableScreenTwo';


function App() {
  return (
   <BrowserRouter>

     <Routes>
       <Route path={'/profile'} element={<ProfileScreen title={'este es el titulo'} subtitle={'este es el subtitulo'} />}/>
       <Route path={'/'} element={<Home/>}/>
       <Route path={'/formulario'} element={<FormScreen/>}/>
       <Route path={'/tabla'} element={<TableScreen/>}/>
       <Route path={'/tablatwo'} element={<TableScreenTwo/>}/>
     </Routes>
   </BrowserRouter>
  );
}

export default App;
