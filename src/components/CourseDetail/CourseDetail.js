import React from "react";
import Price_Icon from '../../assets/icons/ic_baseline-euro-symbol.png'
import People_Icon from '../../assets/icons/people_icon.png'
import Hour_Icon from '/Users/yanina/Documents/Programacion/Netflix_React/netflix-react/src/assets/icons/clock_icon.png'
import Location_Icon from '/Users/yanina/Documents/Programacion/Netflix_React/netflix-react/src/assets/icons/location_icon.png'
import theme from "../../utils/theme";
import backgroundImage from '/Users/yanina/Documents/Programacion/Netflix_React/netflix-react/src/assets/courses/Background_image.png'



const CourseDetail = ({title = '', subtitle = '', coursePrice = '', plazasDisponibles = 0, plazasTotales = 0, couseHours = 0, location = ''}) => {

    return (
        <div style= {{ backgroundImage: `url(${backgroundImage})`,
        height: "400px", backgroundSize:'cover', backgroundRepeat: 'no-repeat', backgroundPosition: 'center',
     display: 'flex', alignItems:'center', justifyContent:'center', color:'white', padding: 40, borderTopLeftRadius: 15, borderBottomLeftRadius: 15,}}>
            
            <div style={{maxwidth: 564, display: 'flex', flexDirection:'column', alignContent: 'center', padding: 30}}>
                <h1 style={theme.h1}>{title}</h1>
                <h2 style={{fontSize: theme.h2.fontSize,  marginTop: -5}}>{subtitle}</h2>

                <div style={{display:'flex', flexDirection:'row'}}>
                    <div style={{display:'flex', flexDirection: 'column', justifyContent:'left', marginTop:10, width:'50%'}}>
                        
                        <div style={{display:'flex', flexDirection: 'row', alignItems:'center', marginBottom:20}}>
                        <img src={Price_Icon} alt='icono de euro' style={{height:16, width:16, objectFit: 'contain',marginRight: 10 }}/>
                        <span style={theme.span}>{coursePrice}</span>
                        </div>

                        <div style={{display:'flex', flexDirection: 'row', alignItems:'center'}}>
                        <img src={Hour_Icon} alt='icono del tiempo' style={{height:16, width:16, objectFit: 'contain',marginRight: 10}}/>
                        <span style={theme.span}>{couseHours}h</span>
                        </div>

                    </div>

                    <div style={{display:'flex', flexDirection: 'column', justifyContent:'left', marginTop:10,}}>
                        
                        <div style={{display:'flex', flexDirection: 'row', alignItems:'center', marginBottom:20}}>
                        <img src={People_Icon} alt='icono de personas' style={{height:16, width:16, objectFit: 'contain', marginRight: 10}}/>
                        <span style={theme.span}>{plazasDisponibles} places disponibles de {plazasTotales}</span>
                        </div>

                        <div style={{display:'flex', flexDirection: 'row', alignItems:'center'}}>
                        <img src={Location_Icon} alt='icono de ubicación' style={{height:16, width:16, objectFit: 'contain', marginRight: 10}}/>
                        <span style={theme.span}>{location}</span>
                        </div>
                       

                    </div>




                </div>
            </div>  



        </div>
    )
};


export default CourseDetail;