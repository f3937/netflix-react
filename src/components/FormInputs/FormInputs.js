import React, {useState} from "react";
import Close_Icon from '/Users/yanina/Documents/Programacion/Netflix_React/netflix-react/src/assets/icons/ic_round-close.png';
import {useNavigate} from 'react-router-dom';
import BreadCrumb from "../BreadCrumb/BreadCrumb";
import theme from "../../utils/theme";
import CustomButton from "../CourseButton/CouseButton";



const FormInputs = () => {
    const  navigate = useNavigate();
    const [email, setEmail] = useState(null);
    const [documentType, setDocumentType] = useState(null);
    const [documentNumb, setDocumentNumb] = useState (null);
    const [ssn, setSsn] = useState(null);
    const [discapacitat, setDiscapacitat] = useState(null);

    const goBack =() => {
        navigate(-1);
    }; 
    const handleEmail = (event) => {
        setEmail(event.target.value);
    };

    const handleDocumentType = (event) => {
        setDocumentType(event.target.value);
    };

    const handleDocumentNumb = (event) => {
        setDocumentNumb(event.target.value)
    };
    const handleSsn = (event) => {
        setSsn(event.target.value);
    };
    
    const handleDiscapacitat = (event) => {
        setDiscapacitat(event.target.value)
    }

const handleNextScreen = () => {
        alert('Has guardado la información del ususario!');
    };




    return ( 
        <div style={{ display:'flex', flexDirection:'column', width:'100%', borderTopRightRadius: 15, borderBottomRightRadius: 15, backgroundColor:'white', padding: 40, position:'relative'}}>

            <button onClick={goBack} style={{ height:24, width:24, border: 'none', background: 'transparent', button: 'none', position: 'absolute', top: 24, right: 40, cursor: 'pointer'}}>
                <img src={Close_Icon} alt='cerrar' style={{height:20, width:20,}} />
            </button>

            <h1 style={{fontSize: 26, fontWeight: '700', textAlign: 'center', width: '100%', marginBottom: 30}}>Preparem la teva inscripció</h1>
            <BreadCrumb position= {0} />

            <h2 style={{fontSize: 16, marginTop:30, fontWeight:'bold'}}>Complimentem el teu perfil</h2>

            <form style={{display:'flex', width: '100%', flexWrap: 'wrap', justifyContent:'space-between' }}>

                 <select style={{width: 250, height: 24, borderRadius: 6, boxSizing: 'content-box', marginBottom: 14, marginRight: 8, padding: 4, border: 'none', backgroundColor: theme.colors.backgroundInputs}} value={documentType} onChange={(event) => handleDocumentType(event)} placeholder={'Tipus de document'}>
                    <option value={'NIF'}>NIF</option>
                    <option value={'NIE'}>NIE</option>
                </select>

                <input style={{width: 250, height: 24, borderRadius: 6, marginBottom: 14, padding: 4 , border: 'none', backgroundColor: theme.colors.backgroundInputs, }} type="text" placeholder={'NIF/NIE'} value={documentNumb} onChange={(event) => handleDocumentNumb(event)}/>

                <input style={{width: 250, height: 24, borderRadius: 6, marginBottom: 14, padding: 4, border: 'none', backgroundColor: theme.colors.backgroundInputs, }} type="text" placeholder={'Núm. Seguretat Social'} value={ssn} onChange={(event) => handleSsn(event)}/>

                <select style={{width: 250, height: 24, borderRadius: 6, boxSizing: 'content-box', marginBottom: 14, padding: 4, border: 'none', backgroundColor: theme.colors.backgroundInputs}} value={discapacitat} onChange={(event) => handleDiscapacitat(event)} placeholder={'Discapacitat'}>
                    <option value={null}>Discapacitat</option>
                    <option value={0}>0%</option>
                    <option value={10}>10%</option>
                    <option value={20}>20%</option>
                    <option value={30}>30%</option>
                </select>

                <input style={{width: '100%', height: 24, borderRadius: 6, marginBottom: 14, padding: 4, border: 'none', backgroundColor: theme.colors.backgroundInputs, }} type="text" placeholder={'Email'} value={email} onChange={(event) => handleEmail(event)}/>

            </form>


            <div style={{width: '100%', display: 'flex', justifyContent: 'center', marginTop: 40, }}>
            <div style={{width: 250, height: 24}}>
                    <CustomButton title={'següent'} onClick={handleNextScreen}/>
                </div>
            </div>










        </div>
    )
};


export default FormInputs;