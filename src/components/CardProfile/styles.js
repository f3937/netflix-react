export default {

profile: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: 6,
},

profilename: {
    color: "grey",
    fontSize: 20,
}
}
