import React from "react"
import styles from "./styles"

const CardProfile = ({profileName, imagesrc}) => {

    return ( 

        <div styles={styles.profile}>
                <img src={imagesrc} />
                <span>{profileName}</span>
        </div>


    )

}


export default CardProfile;
