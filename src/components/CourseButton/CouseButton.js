import React from "react";
import theme from "../../utils/theme";

const CustomButton = ({title, onClick= () => {console.log('clicked!')}}) => {

    return (
        <button style={{border: "none", backgroundColor: theme.colors.primary, width: '100%', height: 40, borderRadius: 8, cursor: 'pointer'}} onClick={onClick}>
        <span style={{fontSize: 16, color: 'white'}}>{title.toUpperCase()}</span>
        </button>
)
}

export default CustomButton;
