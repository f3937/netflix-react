import React from "react"
import theme from "../../utils/theme";

const BreadCrumb = ({position = 0}) => {

    return ( 

        <div style={{height: 16, width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center', position: 'relative', justifyContent: 'space-between'}}>

            <div style={{height: 2, width: '100%', backgroundColor: theme.colors.breadCrumbFilled, zIndex: 0, position: "absolute", top: 8,}}/>        
            <div style={{height: '100%', width: 16, borderRadius: 16, backgroundColor: (position >= 0) ? theme.colors.breadCrumbFilled : 'white', zIndex: 1, border: '2px solid #3744B3', borderColor: theme.colors.breadCrumbFilled}}/>
            <div style={{height: '100%', width: 16, borderRadius: 16, backgroundColor: (position >= 1) ? theme.colors.breadCrumbFilled : 'white', zIndex: 1, border: '2px solid #3744B3', borderColor: theme.colors.breadCrumbFilled}}/>
            <div style={{height: '100%', width: 16, borderRadius: 16, backgroundColor: (position >= 2) ? theme.colors.breadCrumbFilled : 'white', zIndex: 1, border: '2px solid #3744B3', borderColor: theme.colors.breadCrumbFilled}}/>
            <div style={{height: '100%', width: 16, borderRadius: 16, backgroundColor: (position >= 3) ? theme.colors.breadCrumbFilled : 'white', zIndex: 1, border: '2px solid #3744B3', borderColor: theme.colors.breadCrumbFilled}}/>




             </div>


    )

}


export default BreadCrumb;
