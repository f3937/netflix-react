const theme = {
    colors: {
        primary: '#818EFA',
        secondary: '',
        white: '',
        black: 'black', 
        breadCrumbFilled: '#3744B3',
        backgroundInputs: '#F2F2F2'
    },


    h1: {
        fontSize: 26,
    },

    h2: {
        fontSize: 12, 
    },
    
    span: {
        fontSize: 10,
    }, 

    icons: {
        height:24, 
        width:24, 
        objectFit: 'contain',
        color: 'white', 
        padding: 10,

    },

}

export default theme;
