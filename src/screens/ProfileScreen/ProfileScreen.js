import React from "react";
import Logo from "../../assets/netflixLogo/netflixlogo.png";
import styles from "./styles";
import CardProfile from "../../components/CardProfile/CardProfile";
import Profile1 from "../../assets/profile/profile_img1.jpg";
import Profile2 from "../../assets/profile/profile_img2.jpg";
import { useNavigate } from "react-router-dom";



const ProfileScreen =({title, subtitle}) => {
let navigate= useNavigate();
const goToHome =() => {
      navigate("/");
}   

return (
    <>
     <div style={styles.headerContainer} >

        <img  style={styles.logoHeader} src={Logo} alt="logonetflix"/>
    </div>
    <div style={styles.main}>
        <h1 style={styles.h1}> ¿Quién eres? Elige tu perfil </h1>
        <button onClick={goToHome}>
            <span>Cambiar Ruta</span>
        </button>
        <div style={styles.profileContainer}>

            <CardProfile profileName={"Yanina"} imagesrc={Profile1}/>
            <CardProfile profileName={"Cristian"} imagesrc={Profile2}/>
            <CardProfile profileName={"Manuel"}/>
            <CardProfile/>

            </div>
        

    


    </div>








    </>
)
}
export default ProfileScreen;
