export default {

    headerContainer: {
        background: "linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(0,0,0,0) 100%)",
        height: 64, 
        width: "100%"
    },

    logoHeader: {
        width: 100, 
        height: 60, 
        objectFit: "contain", 
        paddingLeft: 20,
    },

    h1: {
        color: "white",
        fontSize: 36,
        alignItems: "center",
    },

    main: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        padding: "10%",
    },

    profileContainer: {
        display: "flex",
        flexDirection: "row",   
    },





}