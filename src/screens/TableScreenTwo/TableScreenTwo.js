import React, {useState} from "react";
import TableHead from "../../components/TableHead/TableHead";
import TableRow from "../../components/TableRow/TableRow";





const TableScreenTwo =() => {


    let headers = [
        'Nombre',
        'Tiempo de cocción',
        'Dificultad',
        'Descripción',
        'Calorías',
        'Raciones'
    ]

    let data = [
        {
            "name": "Pork Bharta",
            "time": 20,
            "difficulty": 1,
            "description": "Boiled pork with onions, chillies, ginger and garlic from Tripura",
            "kCal": 300
        },
        {
            "name": "Chak-Hao Kheer",
            "time": 20,
            "difficulty": 2,
            "description": "Purple rice porridge from Manipur",
            "kCal": 400
        },
        {
            "name": "Galho",
            "time": 20,
            "difficulty": 3,
            "description": "Galho is similar to khichdi, a dish made from rice and also lentils and also popular in the most parts of North East India",
            "kCal": 500
        },
        {
            "name": "Aloo methi",
            "time": 20,
            "difficulty": 2,
            "description": "Potato with fresh Methi leaves.",
            "kCal": 400
        },
        {
            "name": "Aloo shimla mirch",
            "time": 20,
            "difficulty": 3,
            "description": "Green capsicum with potatoes sautéed with cumin seeds, onions, tomatoes, ginger-garlic paste, turmeric, red chilli powder and garam masala",
            "kCal": 500
        }
    ]

    const [listadoDePlatos, setListadoDePlatos] = useState (data);
    const handleAddRow = () => {
        let newListadoDePlatos = [...listadoDePlatos]; // los 3 puntos me clona la variable con una nueva informacion y se resetea cada vez que haga el push
        newListadoDePlatos.push ({name:'test'})
        setListadoDePlatos (newListadoDePlatos)  // otra funcion con una sintesis de nomenclatura

    }


    return (
        <>
            <table style={{borderSpacing: 0}}>
                <thead>
                    <TableHead headers ={headers}/> 
                </thead>
                <tbody>
                    <TableRow listadoDePlatos={listadoDePlatos}/>
                </tbody>
                <button onClick = {handleAddRow}>
                Add
                </button>




            </table>







        </>
    )
}

export default TableScreenTwo;

