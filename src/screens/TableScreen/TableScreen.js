import React, {useState} from "react";
import Pencil  from "../../assets/icons/edit-solid-24.png";

const TableScreen =() =>{

const [comment,  setComment] = useState ('revisar') ;
const [editable, setEditable] = useState (false) ;


const handleComment = (event) => {
    if (editable) {
        setComment (event.target.value);
    } else {
        console.log ('no se puede editar')
         }
    };
    const handleEditable = () => {
        setEditable(!editable);
    }







    return (
<>
     <table>
        <thead>
            <tr style={{backgroundColor:'lightBlue'}}>
                <th style={{width:'40%'}}>Título</th>
                <th> Autor </th>
                <th> Fecha </th>
                <th><img src={Pencil} style={{width:20, height: 20, objectFit:'contain' }}/> </th>
            </tr>

        </thead>

        <tbody>
            <tr>
                    <td>Titulo 1</td>
                    <td>Autor 1</td>
                    <td>Fecha 1</td>
                    <td><input type='text' value={comment} onChange={(event)=> handleComment(event)} placeholder={'add comment'} /></td>
            </tr>
            <tr>
                    <td>Titulo 1</td>
                    <td>Autor 1</td>
                    <td>Fecha 1</td>
                    <td></td>
            </tr>
            <tr>
                    <td>Titulo 1</td>
                    <td>Autor 1</td>
                    <td>Fecha 1</td>
                    <td></td>
            </tr>



        </tbody>


        <tfoot>

        </tfoot>


    </table>
    
    <button onClick={handleEditable}> {editable ? 'Guardar' : 'Editar'} </button>
</>
    )


}
export default TableScreen;

